from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^testapp/$', views.index, name='index'),
    url(r'^testapp/update$', views.update, name='index'),
]
