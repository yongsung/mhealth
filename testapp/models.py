from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Person(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    sensor = models.CharField(max_length=255, default='0000000')
    json_data = models.TextField(default='0000')

    # def is_greater(self):
    #     return self.steps >= self.threshold

