from django.shortcuts import render, render_to_response
from django.http import HttpResponse
from django.template import RequestContext, loader
from django.http import JsonResponse

from rest_framework import viewsets
from rest_framework import permissions
from testapp.models import Person
from testapp.serializers import PersonSerializer

from django.core import serializers

import json

class PersonViewSet(viewsets.ModelViewSet):
    queryset = Person.objects.all()
    serializer_class = PersonSerializer

def index(request):
    last = Person.objects.count()
    person = Person.objects.all().last()
    greetings = None
    # if person.steps >= person.threshold:
    template = loader.get_template('testapp/index.html')
    context = {
        'person': person,
    }
    # if person.is_greater():
    #     greetings = "%s! You are in a good shape! %s, %s, %s" % (person.name, person.steps, person.threshold)
    # else:
    #     sub = person.threshold - person.steps
    #     greetings = "%s! You should lose weight! walk %s more steps" % (person.name, sub)
    return render_to_response('testapp/index.html',{'person':person}, context_instance=RequestContext(request))

def update(request):
    last = Person.objects.count()
    person = Person.objects.filter(pk=last)
    data = serializers.serialize('json', person)

    # return HttpResponse('<h1>Page was found</h1>')
    return HttpResponse(data, content_type='application/json')
# # Create your views here.

